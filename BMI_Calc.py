import random
import csv
import json
import os

commands = ["LIST", "ADD", "DEL", "SELECT"]


# Addition and output function.Outputs graph and some recomendations
def add():

    global ident, users
    # Входные данные
    print("Ваше имя:")
    Name = str(input())
    print("Ваш возраст:")
    Age = int(input())
    print("Ваш пол:")
    Sex = str(input())
    print("Ваш рост,см:")
    Height = int(input())
    print("Ваш вес:")
    Weight = int(input())
    # Входные данные

    # users_data = open('data.txt', 'a+')
    with open("data.txt", "a+", newline="") as users_data:

        BMI = Weight / (Height**2)  # Расчет ИМТ

        # функция вывода рекомендаций и постороения графика

        def recomendation(str):
            condition = ['Недостаточный вес', 'Норма', 'Избыточный вес']
            global plot
            graph_symbol = '-'
            graph_marker = '*'
            i = 0
            print("Пол-", Sex)
            print("Возраст-", Age)
            print("Вес-", Weight)
            print(str)

            # Построение графика
            while i < 23:
                condition.insert(1, graph_symbol)
                condition.insert(3+i, graph_symbol)
                i += 1
            condition.insert(round(BMI*(10**4)), graph_marker)
            print(''.join(condition))
            plot = ''.join(condition)
            # Построение графика

            return plot
        # функция вывода рекомендаций и постороения графика

        # Определение формы обращения в зависимости от пола
        if Sex[0] == "м" or Sex[0] == "m" or Sex[0] == "М" or Sex[0] == "M":
            print("\n")
            print("Уважаемый {}".format(Name))
            print("Ваш возраст:", Age)
            print("Ваш рост:", Height)
            print("Ваш вес:", Weight)
            print("Ваш ИМТ(BMI):", round(BMI*(10**4), 1), "\n")
            if round(BMI*(10**4), 1) < 18.5:
                recomendation(
                    "Недостаточный вес.Правильное питаение. Отказ от вредных привычек.")
            elif 18.5 <= round(BMI*(10**4), 1) <= 22.9:
                recomendation(
                    "Нормальный вес. Для поддержания рекомендуются физические нагрузки")
            else:
                recomendation(
                    "Избыточный вес. Рекомендуется отказ от вредных привычек и занятие спортом.")
        elif Sex[0] == "ж" or Sex[0] == "f" or Sex[0] == "Ж" or Sex[0] == "F":
            print("\n")
            print("Уважаемая {}".format(Name))
            print("Ваш возраст:", Age)
            print("Ваш рост:", Height)
            print("Ваш вес:", Weight)
            print("Ваш ИМТ(BMI):", round(BMI*(10**4), 1), "\n")
            if round(BMI*(10**4), 1) < 18.5:
                recomendation(
                    "Недостаточный вес. Рекомендуется достаточное, регулярное, сбалансированное питание, особенно во время беременности")
            elif 18.5 <= round(BMI*(10**4), 1) <= 22.9:
                recomendation(
                    "Нормальный вес. Для поддержания рекомендутся физические нагрузки")
            else:
                recomendation("Проведите переоценку порций")
        # Определение формы обращения в зависимости от пола
        ident = random.randint(1, 100) # Генерация рандомных id
        users = {
            "id": ident, "value": [Name, Sex, Age, Height, Weight, round(BMI*(10**4), 1), plot]}
        writer = csv.DictWriter(users_data, fieldnames=users) # Запись словаря в csv файл
        #writer.writeheader()
        writer.writerow(users)
        #writer = csv.writer(users_data)
        # writer.writerow(users.items())
        # writer.writerow(users.values())
        # json.dump(users, users_data)
        # users_data.close()
        return ident, plot
# Addition and output function.Outputs graph and some recomendations


# Deletes user from file
def delete():
    with open("data.txt", "r+", newline="") as users_data:
        if os.stat("data.txt").st_size == 0:  # Проверка на наличие в файле информации
            print("\n", "Пользователей нет. Добавьте хотя бы одного пользователя")
        else:
            print("\n", "Введите ID удаляемого пользователя")
            ident = input()
            lines = users_data.readlines()
            users_data.seek(0)
            for i in lines:         # Функция построчного поиска удаляемого пользователя. Поиск по id.
                if ident not in i:
                    users_data.write(i)
            users_data.truncate()
            print("\n", "Пользователь с ID {} удален".format(ident))
            return
# Deletes user from file

# Func which shows or updates info about selected user
def user_select():
    with open("data.txt", "r+", newline="") as users_data:
        print("Введите ID желаемого пользователя")
        ident = input()
        user = []
        for row in csv.reader(users_data):
            i = 0
            if ident in row[i]:
                user.append(row)
                print(user[0])
                break
            i += 1
        print("Желаете обновить информацию?[Y/N]")
        answer = str(input())
        if answer == "Y":
            lines = users_data.readlines()
            users_data.seek(0)
            for i in lines:
                if ident not in i:
                    users_data.write(i)
            users_data.truncate()
            users_data.close()
            add()
    return
# Func which shows or updates info about selected user

while True:
    print("\n", """Введите комманду:\t
    LIST - список пользователей \t
    ADD - добавить пользователя \t
    DEL - удалить пользователя  \t
    SELECT - Выбрать пользователя""")
    cmd_input = input()

    if cmd_input == commands[1]:
        add()
    if cmd_input == commands[0]:
        with open("data.txt", "r", newline="") as users_data:
            reader = csv.reader(users_data) # Считывание и вывод данных из csv файла
            for row in reader:
                print(row[0], " - ", row[1])
    if cmd_input == commands[2]:
        delete()
    if cmd_input == commands[3]:
        user_select()

